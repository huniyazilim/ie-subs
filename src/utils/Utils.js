'use strict';

export const MergeObjects =  (obj1, obj2) => {
    const newObj = {};
    Object.keys(obj1).forEach((key) => {
        newObj[key] = obj1[key];
    });
    Object.keys(obj2).forEach((key) => {
        newObj[key] = obj2[key];
    });
    return newObj;
}