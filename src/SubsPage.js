import React from 'react';
// import $ from 'jquery';  // Nadir, denemeler için, iş bitiminde sil.

import './css/SubsPage.css';

import PageHeader from './components/PageHeader';
import Hero from './components/Hero';
import SubsOptions from './components/SubsOptions';
import SubsOnlyArticles from './components/SubsOnlyArticles';
import ConvinceSection from './components/ConvinceSection';
import Faqs from './components/Faqs';
import PageFooter from './components/PageFooter';

class SubsPage extends React.Component {

    render() {
        return (
            <div className="subs-page">
                <PageHeader/>
                <Hero>
                    <div className="hero__title">
                        <span>IE+</span> SUBSCRIPTION PLANS
                    </div>
                    <div className="hero__cancel">
                        You can <span>cancel anytime.</span>
                    </div>
                    <div className="hero__explanation">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed rutrum nec justo ac tempor.
                        Suspendisse consequat purus lacus, egestas varius mi pharetra
                    </div>
                </Hero>
                <SubsOptions/>
                <SubsOnlyArticles/>
                <ConvinceSection/>
                <Faqs
                    FaqsList = {[
                        {
                            title : "What are your membership options?",
                            answer : "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed rutrum nec justo ac tempor. Suspendisse consequat purus lacus, egestas varius mi pharetra at. Phasellus ullamcorper dui ut fringilla semper."
                        },
                        {
                            title : "How do I obtain a receipt?",
                            answer : "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed rutrum nec justo ac tempor. Suspendisse consequat purus lacus, egestas varius mi pharetra at. Phasellus ullamcorper dui ut fringilla semper."
                        },
                        {
                            title : "How can I change my plan?",
                            answer : "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed rutrum nec justo ac tempor. Suspendisse consequat purus lacus, egestas varius mi pharetra at. Phasellus ullamcorper dui ut fringilla semper."
                        },
                        {
                            title : "Will I still be able to cancel my subscription?",
                            answer : "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed rutrum nec justo ac tempor. Suspendisse consequat purus lacus, egestas varius mi pharetra at. Phasellus ullamcorper dui ut fringilla semper."
                        },
                        {
                            title : "What is the student membership?",
                            answer : "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed rutrum nec justo ac tempor. Suspendisse consequat purus lacus, egestas varius mi pharetra at. Phasellus ullamcorper dui ut fringilla semper."
                        },                        {
                            title : "What are your membership options?",
                            answer : "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed rutrum nec justo ac tempor. Suspendisse consequat purus lacus, egestas varius mi pharetra at. Phasellus ullamcorper dui ut fringilla semper."
                        },                        {
                            title : "I have more questions.",
                            answer : "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed rutrum nec justo ac tempor. Suspendisse consequat purus lacus, egestas varius mi pharetra at. Phasellus ullamcorper dui ut fringilla semper."
                        },
                    ]}
                />
                <PageFooter/>
            </div>
        )
    }

}


export default SubsPage;