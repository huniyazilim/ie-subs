import React from 'react';
import ReactDOM from 'react-dom';
import './css/index.css';
import SubsPage from './SubsPage';

ReactDOM.render(
  <React.StrictMode>
    <SubsPage />
  </React.StrictMode>,
  document.getElementById('root')
);