import React from 'react';

class HrHeader extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        const {Title, HrColor, TitleColor} = this.props;
        return (
            <div className="hr-header">
                <label className="hr-header__title" style={{color: TitleColor || "#007FC0"}}>{Title}</label>
                <div className="hr-header__hr" style={{backgroundColor: HrColor || "#FF9900"}}></div>
            </div>
        )

    }

}

export default HrHeader;