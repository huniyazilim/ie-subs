import React from 'react';

import rocket from '../images/rocket.svg';

class PageFooter extends React.Component {

    render() {
        return (
            <div className="page-footer">
                <img className="page-footer__rocket" src={rocket} alt="rocket"></img>
                <div className="newsletter-form">
                    <div className="newsletter-content">
                        <p className="newsletter-content__title">JOIN OUR NEWSLETTER</p>
                        <p className="newsletter-content__detail">Stay on top of the latest engineering news just enter your email and we’ll take care of the rest.</p>
                    </div>
                    <form className="newsletter-form__form-items">
                        <input className="newsletter-form__input" type="text" placeholder="Enter your email here"></input>
                        <button className = "newsletter-form__button">SIGN UP</button>
                    </form>

                </div>
            </div>
        )

    }

}

export default PageFooter;