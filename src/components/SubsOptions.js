import React from 'react';

import Tabs from '../common/Tabs';
import SubsList from './SubsList';

import paperPlane from '../images/paper-plane.svg';
import paperPlaneSelected from '../images/paper-plane--selected.svg';
import plane from '../images/plane.svg';
import planeSelected from '../images/plane--selected.svg';
import ufo from '../images/ufo.svg';
import ufoSelected from '../images/ufo--selected.svg';

class SubsOptions extends React.Component {

    GetTabs() {
        return [
            {
                label : "STUDENT",
                customLiStyle : {borderRadius : "10px 0 0 0", borderRight : "1px solid #C7C7C7"},
                titleSvg : paperPlane,
                activeTitleSvg : paperPlaneSelected,
                content : <SubsList
                            Items = {
                                [
                                    {
                                        header : "Monthly",
                                        explanation : "Full access to all features and usnlimited product updates. Speacial discounts.",
                                        currency : "$",
                                        price : "3",
                                        showMoreLink : "#student-monthly",
                                        isPopular : true
                                    },
                                    {
                                        header : "Quarterly",
                                        explanation : "Full access to all features and usnlimited product updates. Speacial discounts.",
                                        currency : "$",
                                        price : "12",
                                        showMoreLink : "#student-quarterly"
                                    },
                                    {
                                        header : "Annual",
                                        explanation : "Full access to all features and usnlimited product updates. Speacial discounts.",
                                        currency : "$",
                                        price : "24",
                                        showMoreLink : "#student-annual"
                                    },
                                ]
                            }
            />
            },
            {
                label : "INDIVIDUAL",
                titleSvg : plane,
                activeTitleSvg : planeSelected,
                content : <SubsList
                            Items = {
                                [
                                    {
                                        header : "Monthly",
                                        explanation : "Full access to all features and usnlimited product updates. Speacial discounts.",
                                        currency : "$",
                                        price : "5",
                                        showMoreLink : "#individual-monthly"
                                    },
                                    {
                                        header : "Quarterly",
                                        explanation : "Full access to all features and usnlimited product updates. Speacial discounts.",
                                        currency : "$",
                                        price : "18",
                                        isPopular : true,
                                        showMoreLink : "#individual-quarterly"
                                    },
                                    {
                                        header : "Annual",
                                        explanation : "Full access to all features and usnlimited product updates. Speacial discounts.",
                                        currency : "$",
                                        price : "35",
                                        showMoreLink : "#individual-annual"
                                    },
                                ]
                            }
                        />
            },
            {
                label : "ENTERPRISE",
                titleSvg : ufo,
                activeTitleSvg : ufoSelected,
                customLiStyle : {borderRadius : "0 10px 0 0", borderLeft : "1px solid #C7C7C7"},
                content : <SubsList
                            Items = {
                                [
                                    {
                                        header : "Monthly",
                                        explanation : "Full access to all features and usnlimited product updates. Speacial discounts.",
                                        currency : "$",
                                        price : "25",
                                        showMoreLink : "#enterprise-monthly"
                                    },
                                    {
                                        header : "Quarterly",
                                        explanation : "Full access to all features and usnlimited product updates. Speacial discounts.",
                                        currency : "$",
                                        price : "40",
                                        showMoreLink : "#enterprise-quarterly"
                                    },
                                    {
                                        header : "Annual",
                                        explanation : "Full access to all features and usnlimited product updates. Speacial discounts.",
                                        currency : "$",
                                        price : "80",
                                        isPopular : true,
                                        showMoreLink : "#enterprise-annual"
                                    },
                                ]
                            }
            />
            }
        ]
    }

    render() {
        return (
            <div className="subs-options">
                <div className="subs-options__tabs">
                    <Tabs
                        Tabs = {this.GetTabs()}
                        ActiveTab = "INDIVIDUAL"
                        TabHeaderClassName = "subs-options__tabs-headers"
                        ActiveTabClassName = "subs-options__tabs-headers--selected"
                        UlClassName = "subs-options__ul"
                        OuterDivClassName = "subs-options__outer"
                    />
                </div>
            </div>
        )

    }

}

export default SubsOptions;