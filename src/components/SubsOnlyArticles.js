import React from 'react';

import HrHeader from './HrHeader';

class ArticleItem extends React.Component {

    constructor(props) {
        super(props);
    }

    HandleImageClick(){
        console.log("Article image clicked.");
        // handle image click action here
    }

    HandleGenreClick() {
        console.log("Article genre clicked.");
        // handle genre click action here
    }

    render() {
        const {ImageUrl, GenreColor, AuthorName, PublishDate, GenreName, Title} = this.props;
        return (
            <div className="article-item">
                <img className="article-item__img" onClick={this.HandleImageClick.bind(this)} src={ImageUrl}></img>
                <div className="article-item__details">
                    <p className="article-item__genre"
                        onClick={this.HandleGenreClick.bind(this)} style={{color: GenreColor || "#0180C1"}}>
                        {GenreName}
                    </p>
                    <p className="article-item__title"
                        onClick={this.HandleImageClick.bind(this)}>
                            {Title}
                        </p>
                    <p className="article-item__meta">
                        <span className="article-item__author-name">{AuthorName}</span> |
                        <span className="article-item__publish-date">{PublishDate}</span>
                    </p>
                </div>
            </div>
        )
    }
}

class SubsOnlyArticles extends React.Component {

    render() {
        return (
            <div className="subs-only-articles">
                <HrHeader Title = "SUBSCRIBER ONLY ARTICLES" HrColor="#FF9900" />
                <div className="subs-only-articles__article-items">
                    <ArticleItem
                        ImageUrl="../images/covid-girl.png"
                        GenreName = "SCIENCE"
                        GenreColor="#0EB58D"
                        Title = "8 founders, leaders highlight fintech and deep tech as Bristol’s top sectors"
                        AuthorName = "Mike Paul Hunter"
                        PublishDate = "20 hrs ago"
                    />
                    <ArticleItem
                        ImageUrl="../images/asimo.png"
                        GenreName = "CULTURE"
                        GenreColor="#C23824"
                        Title = "Imperiled for helping U.S. troops and stranded by bureaucracy, Afghan interpreters see Biden.."
                        AuthorName = "Mike Paul Hunter"
                        PublishDate = "20 hrs ago"
                    />
                    <ArticleItem
                        ImageUrl="../images/mag-lev.png"
                        GenreName = "TRANSPORTATION"
                        GenreColor="#750EB5"
                        Title = "8 founders, leaders highlight fintech and deep tech as Bristol’s top sectors"
                        AuthorName = "Mike Paul Hunter"
                        PublishDate = "20 hrs ago"
                    />
                </div>
            </div>
        )

    }

}

export default SubsOnlyArticles;