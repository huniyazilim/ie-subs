import React from 'react';

import HrHeader from './HrHeader';

import plus from '../images/plus.svg';

class FaqItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isAnswerVisible : false
        }
    }

    HandleHeaderClick() {
        this.setState({isAnswerVisible : !this.state.isAnswerVisible});
    }

    render() {
        const {Title} = this.props;
        const titleClass = this.state.isAnswerVisible ? "faq-item__title faq-item__title--answer-visible" : "faq-item__title";
        const iconClass = this.state.isAnswerVisible ? "faq-item__icon faq-item__icon--answer-visible" : "faq-item__icon";
        return (
            <div className="faq-item">
                <div className="faq-item__header" onClick={this.HandleHeaderClick.bind(this)}>
                    <div className={titleClass}>{Title}</div>
                    <img className={iconClass} src={plus} alt="plus"/>
                </div>
                {
                    this.state.isAnswerVisible ?
                        <div className="faq-item__answer">{this.props.children}</div>
                    : null
                }
            </div>
        )

    }

}

class Faqs extends React.Component {

    constructor(props) {
        super(props);
    }
    
    render() {
        const {FaqsList} = this.props;
        return (
            <div className="faqs">
                <HrHeader Title = "FAQs" HrColor="#FF9900"/>
                <div className="faqs__list">
                    { FaqsList.map((faq, idx) => <FaqItem key={"faqs_"+idx} Title={faq.title}>{faq.answer}</FaqItem>) }
                </div>

            </div>
        )

    }

}

export default Faqs;