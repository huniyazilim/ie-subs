import React from 'react';

class ConvinceSection extends React.Component {

    render() {
        return (
            <div className="convince-section">
                <div className="convince-section__title">
                    Why Subscribe to Interesting Engineering USP Points come here.
                </div>
                <div className="convince-section__explanation">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed rutrum nec justo ac tempor. Suspendisse consequat purus lacus, egestas varius mi pharetra at. Phasellus ullamcorper dui ut fringilla semper.
                </div>
            </div>
        )

    }

}

export default ConvinceSection;