import React from 'react';

import mic from '../images/mic.svg';
import search from '../images/search.svg';

class PodcastSearch extends React.Component{

    HandleSearchIconClick() {
        // Handle Search podcast icon click here.
        console.log("Search podcast icon clicked.");
    }

    render() {
        return (
            <div className="podcast-search">
                <a href="#">
                    <img src={mic} className="podcast-search__mic" alt="mic" />
                </a>
                <label className="podcast-search__title">PODCAST</label>
                <a href="#">
                    <img src={search} className="podcast-search__search" alt="search" />
                </a>
            </div>
        )

    }

}


export default PodcastSearch;