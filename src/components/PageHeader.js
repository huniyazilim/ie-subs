import React from 'react';

import PodcastSearch from './PodcastSearch';

import menu from '../images/menu.svg';
import ieLogo from '../images/ie-logo.svg';

class PageHeader extends React.Component{

    OnToggleMenu(e) {
        e.preventDefault();
        // Toggle menu logic will be here
    }

    HandleSubcribeButtonClick() {
        // Handle subscription button click here
        console.log("Subscribe button clicked...");
    }

    render() {
        return (
            <header className="page-header">
                <div className="page-header__left-section">
                    <a href="#" onClick={this.OnToggleMenu.bind(this)}>
                        <img src={menu} className="page-header__menu" alt="menu" />
                    </a>
                    <a href="https://www.interestingengineering.com" target="_blank">
                        <img src={ieLogo} className="page-header__ie-logo" alt="ie-logo" />
                    </a>
                </div>
                <div className="page-header__right-section" onClick={this.HandleSubcribeButtonClick.bind(this)}>
                    <button className="page-header__subscribe-button">SUBSCRIBE</button>
                    <PodcastSearch></PodcastSearch>
                </div>

            </header>
        )

    }

}


export default PageHeader;