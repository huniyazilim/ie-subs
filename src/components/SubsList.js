import React from 'react';

class SubsListItem extends React.Component {

    constructor(props) {
        super(props);
    }

    HandleSubscribeButtonClick() {
        if (this.props.OnSubscribe) {
            const {Header, Currency, Price} = this.props;
            this.props.OnSubscribe(Header, Currency, Price);
        }
    }

    render() {
        const {Header, Explanation, Currency, Price, ShowMoreLink, IsPopular} = this.props;
        return (
            <div className="subs-list-item">
                {
                    IsPopular ? <div className="subs-list-item__most-popular">MOST POPULAR</div> : null
                }
                <div className="subs-list-item__col-1">
                    <h3 className="subs-list-item__header">{Header}</h3>
                    <p className="subs-list-item__explanation">{Explanation}</p>
                </div>
                <div className="subs-list-item__col-2">
                    <p className="subs-list-item__price-tag">{Currency}{Price}</p>
                </div>
                <div className="subs-list-item__col-3">
                    <p className="subs-list-item__price-tag--ss">{Currency}{Price}</p>
                    <button onClick={this.HandleSubscribeButtonClick.bind(this)} className="subs-list-item__subscribe-button">SUBSCRIBE</button>
                    <a className="subs-list-item__show-more" href={ShowMoreLink}>Show more</a>
                </div>
            </div>
        )

    }

}

class SubsList extends React.Component {

    render() {
        const { Items } = this.props;

        return (
            <div className="subs-list">

                {
                    Items.map(item => <SubsListItem
                            Header = {item.header}
                            Explanation = {item.explanation}
                            Price = {item.price}
                            Currency = {item.currency}
                            ShowMoreLink = {item.showMoreLink}
                            IsPopular = {item.isPopular}
                        />)
                }

            </div>
        )

    }

}

export default SubsList;