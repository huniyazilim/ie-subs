import React from "react";

class Tab extends React.Component{

  handleClick(){
    if (this.props.OnTabClick){
      this.props.OnTabClick(this.props.Label);
    }
  }

  render() {
    const {TabHeaderClassName, ActiveTabClassName} = this.props;
    const tabHeaderClassName = this.props.isActiveTab ? TabHeaderClassName + " " + ActiveTabClassName : TabHeaderClassName;
    const titleSvg = this.props.isActiveTab ? this.props.ActiveTitleSvg: this.props.TitleSvg;
    return (
      <li className={tabHeaderClassName} style={this.props.CustomLiStyle} onClick={this.handleClick.bind(this)}>
        {
            this.props.TitleSvg ?
            <img src = {titleSvg}/>
            : null
        }
        <label>{this.props.Label}</label>
      </li>
    );
  }
}

class Tabs extends React.Component {
  constructor(props){
      super(props);
      this.state = {
          activeTab : props.ActiveTab || props.Tabs[0].label,
      };
  }

  handleTabClick(label){
    this.setState({activeTab:label});
  }

  getTabContent() {
    return this.props.Tabs.filter(t => t.label == this.state.activeTab)[0].content;
  }

    render(){
        const tabContent = this.getTabContent();
        const { UlStyle } = Styles;
        const { UlClassName, OuterDivClassName } = this.props;
        return (
          <div className = {OuterDivClassName}>

            <ul style={UlStyle} className={UlClassName}>
              {this.props.Tabs.map((tab, idx) => {
                const isActiveTab = tab.label == this.state.activeTab;
                return  <Tab key={idx} isActiveTab = {isActiveTab}
                            Label={tab.label}
                            OnTabClick={this.handleTabClick.bind(this)}
                            {...this.props}
                            CustomLiStyle = {tab.customLiStyle}
                            TitleSvg = {tab.titleSvg}
                            ActiveTitleSvg = {tab.activeTitleSvg}
                        />
              })}
            </ul>

            <div>
              {tabContent}
            </div>

          </div>
        );
    }
}

const Styles = {
    UlStyle : {
        display : "flex",
        listStyle : "none"
    }
}

export default Tabs;
